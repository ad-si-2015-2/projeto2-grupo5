package example.hello;
import java.rmi.Naming;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.rmi.server.UnicastRemoteObject;
	
public class Server extends UnicastRemoteObject implements ServerInterface {
	
    private ArrayList<ClientInterface> jogadores;
    
    public Server() throws RemoteException {
        
    }
    
    public String sayHello() {
	  return "Hello, world! ";
    }
    
    public void registrarJogador(ClientInterface jogador) throws RemoteException
    {
        this.jogadores.add(jogador);
        jogador.notificarJogador("Você entrou no jogo");
        jogador.notificarJogador("Cliente registrado com sucesso!");
        jogador.notificarJogador("Comandos: /listar, /criar, /rooms, /entrar, /iniciar (apenas se estiver na room)");
    }
    
    public static void main(String args[]) {
	
	try {
        LocateRegistry.createRegistry(1099); 
	    Server obj = new Server();
        
        Naming.rebind("//localhost/RmiServer", obj);

	    System.err.println("Server ready");
	} catch (Exception e) {
	    System.err.println("Server exception: " + e.toString());
	    e.printStackTrace();
	}
    }
}