package example.hello;

import java.rmi.Naming;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.UUID;


public class Client implements ClientInterface {
	
	static ServerInterface server;
	static BufferedReader leitor = new BufferedReader(new InputStreamReader(System.in));
	private String nome;
	private UUID id;
	
    private Client() {}
	
    public static void main(String[] args) throws Exception {
		String host = (args.length < 1) ? "localhost:1099" : args[0];
		try {
			ServerInterface server = (ServerInterface)Naming.lookup("//localhost/RmiServer");
			String response = server.sayHello();
			
			System.out.println("response: " + response);
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
		
		System.out.println("Informe seu nome: ");

        Client cliente = new Client();
        cliente.nome = leitor.readLine();
        cliente.id = UUID.randomUUID();

        ClientInterface stub = (ClientInterface) UnicastRemoteObject.exportObject(cliente, 0);

        Random gerador = new Random();
        Registry registry = LocateRegistry.createRegistry(gerador.nextInt(1000) + 6000);
        registry.bind("Client", stub);
		
		server.registrarJogador(cliente);
		
    }
	

	@Override
    public void notificarJogador(String mensagem) throws RemoteException {
        System.out.println(mensagem);
    }

}