package example.hello;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientInterface extends Remote {
    public void notificarJogador(String mensagem) throws RemoteException;
}