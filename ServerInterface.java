package example.hello;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
  public String sayHello() throws RemoteException;
  public void registrarJogador(ClientInterface jogador) throws RemoteException;
}